module Api::V1
  class SpotsController < ApplicationController

    def index
      render json: Spot.all
    end

    def create
      @spot = Spot.new(spot_params)
      if @spot.save
        render json: @spot, status: :created
      else
        render json: { errors: @spot.errors.full_messages }, status: 422
      end
    end

    def update
      @spot = Spot.find(params[:id])
      if @spot.update_attributes(spot_params)
        render json: @spot, status: :ok
      else
        render json: @spot.errors.full_messages, status: :unprocessable_entity
      end
    end

    def destroy
      @spot = Spot.find(params[:id])
      if @spot.destroy
        render json: :no_content, status: :ok
      else
        render json: @spot.errors.full_messages, status: :unprocessable_entity
      end
    end

    private

    def spot_params
      params.require(:spot).permit(:name, :position_x, :position_y)
    end

  end
end
