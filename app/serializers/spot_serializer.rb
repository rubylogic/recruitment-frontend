class SpotSerializer < ActiveModel::Serializer
  attributes :id, :name, :position_x, :position_y
end