class Spot < ApplicationRecord
  validates :name, :position_x, :position_y, presence: true
  validates :position_x, :position_y, inclusion: { in: 0..2000}
  validates :name, length: {minimum: 3, maximum: 20}
end
