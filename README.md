### Requirements:
Ruby 2.2.2+

# Installation:
  * Install **Ruby** ([this link covers major operating systems](https://gorails.com/setup/ubuntu/16.04#ruby) - **Installing Ruby** section should be sufficient, you don't need to go through other steps)
  * Install **Git**
  * `git clone git@bitbucket.org:rubylogic/recruitment-frontend.git`
  * `cd frontend-recruitment`  (goes to project directory)
  * `bundle install`  (installs all gems including Rails)
  * `rails db:setup`  (prepares database)
  * `rails s`  (starts rails server on port 3000)
  
####Caution
This as an API application - it is not supposed to work via web browser and when you go to http://localhost:3000 you will see standard Rails Welcome Screen. However you will be able to interact with it via JSON API.   

# API:

Application provides simple JSON API accessible at <http://localhost:3000/api/v1/spots>. 
Spots attributes (all required):
  * name (string), min: 3 characters, max: 20 characters
  * position_x (integer), min: 0, max: 2000
  * position_y (integer), min: 0, max: 2000
  
### API Methods:
Here you can find samples of API Methods usage (via curl).  

##### INDEX:
`curl localhost:3000/api/v1/spots`

##### CREATE:
`curl -H "Content-Type: application/json" -X POST -d '{"name":"xyz", "position_x":500, "position_y":600}' http://localhost:3000/api/v1/spots`

##### UPDATE:
`curl -H "Content-Type: application/json" -X PATCH -d '{"name":"patched", "position_x":500, "position_y":600}' http://localhost:3000/api/v1/spots/2` (or any existing spot ID)

##### DESTROY:
`curl -H "Content-Type: application/json" -X DELETE http://localhost:3000/api/v1/spots/2` (or any existing spot ID)


