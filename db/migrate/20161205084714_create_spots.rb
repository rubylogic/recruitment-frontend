class CreateSpots < ActiveRecord::Migration[5.0]
  def change
    create_table :spots do |t|
      t.string :name
      t.integer :position_x
      t.integer :position_y

      t.timestamps
    end
  end
end
