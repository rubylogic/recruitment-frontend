require 'rails_helper'

RSpec.describe Spot, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:position_x) }
  it { should validate_presence_of(:position_y) }

  it { should validate_inclusion_of(:position_x).in_range(0..2000) }
  it { should validate_inclusion_of(:position_y).in_range(0..2000) }

  it {should validate_length_of(:name).is_at_least(3).is_at_most(20) }
end