require 'rails_helper'

RSpec.describe Api::V1::SpotsController, type: :controller do

  describe 'index' do
    it { get 'index'; expect(response).to have_http_status(:success)}
  end

  describe 'create' do
    context 'with valid attributes' do
      it 'creates the spot' do
        original_spot_count = Spot.count
        Spot.create(name: 'alala', position_x: 200, position_y: 400)
        expect(Spot.count).to eq(original_spot_count+1)
      end
    end
    context 'with invalid attributes' do
      it 'does not create the spot' do
        original_spot_count = Spot.count
        Spot.create(name: 'alala')
        expect(Spot.count).to eq(original_spot_count)
      end
    end
  end

  describe 'update' do
    a = Spot.create(name: 'alala', position_x: 200, position_y: 400)
    it 'updates a record' do
      a.update(name: 'ba', position_x: 300, position_y: 500)
      a.save
      expect(a.name).to eq('ba')
      expect(a.position_x).to eq(300)
      expect(a.position_y).to eq(500)
    end
  end

  describe 'destroy' do
    a = Spot.create(name: 'alala', position_x: 200, position_y: 400)
    original_spot_count = Spot.count
    it 'deletes a record' do
      a.delete
      expect(Spot.count).to eq(original_spot_count-1)
    end
  end

end